#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
double delta_t, motor_speed_change, heading, Kp, error;
static double O_distance, O_speed;
int trigPin = 11;    // Trigger
int echoPin = 12;    // Echo
long duration, cm, inches;
double BaseReading;
double writetime, n, sumvalues, a,Y_bottom, Y_top, R_bottom, R_top ;

int runattempt;

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

// Making two motor objects for left and right motors, connected to M1 nnd M2 respectively
Adafruit_DCMotor *myMotorL = AFMS.getMotor(2);
Adafruit_DCMotor *myMotorR = AFMS.getMotor(1);
void setup(void) 
{
  Serial.begin(9600);
  Serial.println("Algorithm Test for Motors Only!");
  AFMS.begin();  // create with the default frequency 1.6KHz
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  O_speed = 150;
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  O_distance = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Original Distance: ");
  Serial.println(O_distance);
}
int sort_desc(const void *cmp1, const void *cmp2)
  {
  // Need to cast the void * to int *
  int a = *((int *)cmp1);
  int b = *((int *)cmp2);
  // The comparison
  return a > b ? -1 : (a < b ? 1 : 0);
  // A simpler, probably faster way:
  //return b - a;
  }

int mine_colour() {
  
  writetime = 250;
  n = 5;
  int colourvalues[250] = { };
  
  for (int i=0; i < writetime; i++){
    int colourvalue = analogRead(A0);
    colourvalues[i] = colourvalue;
    delay(2);
  }

  // Number of items in the array
  int arr_length = sizeof(colourvalues) / sizeof(colourvalues[0]);
  // qsort - last parameter is a function pointer to the sort function
  qsort(colourvalues, arr_length, sizeof(colourvalues[0]), sort_desc);
  // lt is now sorted

  sumvalues = colourvalues[0] + colourvalues[1] + colourvalues[2] + colourvalues[3] + colourvalues[4];
  
  int final_value = sumvalues/5;
  return final_value;
  
}

void drive(int timer){

  for (int i=0; i < timer; i++){
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Distance: ");
  Serial.println(cm);

//P Control
  Kp = 3;
  error = cm - O_distance;
  motor_speed_change = Kp * error;

  Y_bottom = 900;
  Y_top = 1100;
  R_bottom = 780;
  R_top = 870;
  BaseReading = 700;
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  if (sensorValue > BaseReading) {
  myMotorL->setSpeed(30);
  myMotorR->setSpeed(30);
  myMotorL->run(BACKWARD); 
  myMotorR->run(BACKWARD); 

  int a = mine_colour();
  delay(500);
    Serial.println(a);
  if (Y_bottom < a && a < Y_top){
    Serial.println("yellow"); 
  }
  else if (R_bottom < a && a < R_top){
    Serial.println("Red"); 
  }
  break;
  }
  else {
  myMotorL->run(BACKWARD); 
  myMotorR->run(BACKWARD); 

  myMotorL->setSpeed(O_speed + motor_speed_change);
  myMotorR->setSpeed(O_speed - motor_speed_change);

  Serial.print("Motor Speed");
  Serial.println(motor_speed_change);
  delay(250);
  }
}
}
void AllStop() {
 myMotorL->run(RELEASE); // turns off left motor
 myMotorR->run(RELEASE); // turns off right motor
 Serial.println("Stop!");
}

void loop(){

 myMotorL->setSpeed(O_speed);
 myMotorR->setSpeed(O_speed);

 drive(100);
 AllStop();
 delay(10000);


}
