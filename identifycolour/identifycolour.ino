// decalre global variables, THESE SHOULD APPEAR AT THE VERY BEGINNING OF THE CODE----------------------------------
int lowred = 350;
int upred = 450;
int lowyellow = 150;
int upyellow = 250;
String colour;
// check with electronics the red and yelloe Led pin numbers ---------------------------------------------------------
int redLed = 13;
int yellowLed = 15;
int LSensor = 0;
int CSensor = 1;
int RSensor = 2;


void setup() {
  // setup the input pins and output pins corresponding to the different sensors 
  pinMode(lowred, INPUT);
  pinMode(upred, INPUT);
  pinMode(lowyellow,INPUT);
  pinMode(upyellow, INPUT);
  pinMode(redLed, OUTPUT);
  pinMode(yellowLed,OUTPUT);
  pinMode(LSensor,INPUT);
  pinMode(CSensor,INPUT);
  pinMode(RSensor,INPUT);
}

// createt a function which passes location of sensors (left, center or right) and wavelength of the colour detectetd
String identify_Lcolour() {
  if (analogRead(LSensor) > lowred && analogRead(LSensor) < upred){
    colour = 'red';
    digitalWrite (redLed, HIGH);
    delay (5000);
    digitalWrite (redLed, LOW);
    Serial.print(colour);
    }
  else if ((analogRead(LSensor) > lowyellow && analogRead(LSensor) < upyellow){
    colour = 'yellow';
    digitalWrite (redLed, HIGH);
    delay (5000);
    digitalWrite (redLed, LOW);
    Serial.print(colour);
    }
}

String identify_Ccolour {
  if (analogRead(CSensor) > lowred && analogRead(CSensor) < upred){
    colour = 'red';
    digitalWrite (redLed, HIGH);
    delay (5000);
    digitalWrite (redLed, LOW);
    Serial.print(colour);
    }
  else if ((analogRead(CSensor) > lowyellow && analogRead(CSensor) < upyellow){
    colour = 'yellow';
    digitalWrite (redLed, HIGH);
    delay (5000);
    digitalWrite (redLed, LOW);
    Serial.print(colour);
    }
}

String identify_Rcolour {
  if (analogRead(RSensor) > lowred && analogRead(RSensor) < upred){
    colour = 'red';
    digitalWrite (redLed, HIGH);
    delay (5000);
    digitalWrite (redLed, LOW);
    Serial.print(colour);
    }
  else if ((analogRead(RSensor) > lowyellow && analogRead(RSensor) < upyellow){
    colour = 'yellow';
    digitalWrite (redLed, HIGH);
    delay (5000);
    digitalWrite (redLed, LOW);
    Serial.print(colour);
    }
}
