//highest level of code
//include various header file(s)


int main() {
	//write a function for going to initial start position from start box
	go_to_start_position1();
	//define an iterator "walls_completed"
	walls_completed = 0
		while (walls_completed < 4) {
			//write a function for moving forwards (at a steady speed)
			steady_forwards();
			//write a boolean for if it reaches the other side of the zone
			if (reached_other_side == true) {
				//write a float/integer for step size
				step = 20
			}
			else {
				//write boolean for if a mine is detected by any sensors
				if (mine_detected == true) {
					//write a boolean for if it is only detected on the left hand side
					if (LHS_sensor_only == true) {
						//write a function to stop the car
						stop();
						//write a function to identify the colour that returns "red" or "yellow" and tells where to store information that follows? - and makes the appropriate lights turn on
						if (identify_colour() == "red") {
							//write a function to record mine coordinates (if identify_colour allows you to tell it to store the coordinates for the correct colour - otherwise make two different functions for each colour)
							record_mine_coordinates();
						}
						else if (identify_colour() == "yellow") {
							record_mine_coordinates();
						}
						step = 20;
						//write a function to reverse out of the search zone, and stop once it is safely out of the search zone
						reverse_out_of_zone();
					}
					else {
						//write a boolean for if it is only detected on the right hand side
						if (RHS_sensor_only == true) {
							stop();
							if (identify_colour() == "red") {
								record_mine_coordinates();
							}
							else if (identify_colour() == "yellow") {
								record_mine_coordinates();
							}
							step = 10;
							reverse_out_of_zone();
						}
						else {
							step = 20;
							stop();
							if (identify_colour() == "yellow") {
								record_mine_coordinates();
								//write a function that drives the remaining distance to the mine in order to collect it
								drive_to_yellow_mine();
								//write a function to close gates to collect the yellow mine
								close_gates();
								reverse_out_of_zone();
								//write a function to open the gates to release the yellow mine outside of the search zone - make this function check that the mine would be safely out of the search zone when the mine is deposited
								open_gates();
							}
							else if (identify_colour() == "red") {
								record_mine_coordinates();
								reverse_out_of_zone();
							}
						}
					}
				}
				reverse_out_of_zone();
				//write a function that tells you if you have completed that wall
				if (wall_complete() == true) {
					walls_completed += 1;
					go_to_start_position_new_wall();
				}
				else {
					//write a function that tells the robot to move to the right by step size "step" (unless this is implemented in the reversing function)
					move_to_right(step);
				}
				//write a function that tells the robot to return to the start box once the algorith has run
				return_to_start_box();
			}
		}
}
