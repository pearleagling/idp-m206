//just need to test Identify Colour function then can add it and then we can test this!

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

double delta_t, motor_speed_change, heading, Kp, Ki, Kd, error,  accumulatederror, derror, lasterror;
static double O_distance, O_speed;
int trigPin = 11;    // Trigger
int echoPin = 12;    // Echo
int trigPinrear = 8;    // Triggerrear
int echoPinrear = 9;    // Echorear
long duration, cm, inches, durationrear,cmrear;

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
// Making two motor objects for left and right motors, connected to M1 nnd M2 respectively
Adafruit_DCMotor *myMotorL = AFMS.getMotor(2);
Adafruit_DCMotor *myMotorR = AFMS.getMotor(1);

// insert function for SteadyForward()
void SteadyForward() {
 //for (int i=0; i < timer; i++){
 // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
 // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  myMotorL->run(BACKWARD); 
  myMotorR->run(BACKWARD); 
  myMotorL->setSpeed(O_speed);
  myMotorR->setSpeed(O_speed);
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  
  
  Serial.print("Distance: ");
  Serial.println(cm);
  //only carry out proportional control when the reading is valid, i.e.smaller than 300.
  error = cm - O_distance;
  
  if (cm < 400){
    if (abs(cm-O_distance) <= 5){
      Kp = 3;
      }
    else{
      Kp = 5;
      }
  // integral and derivative added here.
  accumulatederror += error; // add current error to running total of error
  // unsure about the windup prevention here, but since we have already set cm<400, this should be fine -------------------------------------------------------------------------------------
  //Kp and Kd value should be bigger than Ki value.  
  derror = error - lasterror; // derivative error is the error difference between current error and last error 
  Ki = 0;
  Kd = 10;
  
  motor_speed_change = Kp * error + Ki * accumulatederror + Kd * derror;
  
  lasterror = error;
  
  myMotorL->setSpeed(O_speed + motor_speed_change);
  myMotorR->setSpeed(O_speed - motor_speed_change);
  Serial.print("Motor Speed change");
  Serial.println(motor_speed_change);
  delay(250);
  }
  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear = pulseIn(echoPinrear, HIGH);
  cmrear = (durationrear/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Distancerearforward: ");
  Serial.println(cmrear);
}


//insert function for ReverseOut()
void ReverseOut(){
  //for (int i=0; i < timer; i++){
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  myMotorL->run(FORWARD); 
  myMotorR->run(FORWARD); 
  myMotorL->setSpeed(O_speed);
  myMotorR->setSpeed(O_speed);
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  
  //durationrear = pulseIn(echoPinrear, HIGH);
  
  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Distance: ");
  Serial.println(cm);
  //cmrear = (durationrear/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  //Serial.print("Distancerear: ");
  //Serial.println(cmrear);
  
if (cm <400){
  //P Control
  if (abs(cm-O_distance) <= 5){
    Kp = 3;
    }
  else{
    Kp = 5;
    }
  error = cm - O_distance;
  // integral and derivative added here.
  accumulatederror += error; // add current error to running total of error
// unsure about the windup prevention here, but since we have already set cm<400, this should be fine -------------------------------------------------------------------------------------
//Kp and Kd value should be bigger than Ki value.  
  derror = error - lasterror; // derivative error is the error difference between current error and last error 
  Ki = 0;
  Kd = 10;
  
  motor_speed_change = Kp * error + Ki * accumulatederror + Kd * derror;
  
  lasterror = error;
  motor_speed_change = Kp * error + Ki * accumulatederror + Kd * derror;
  myMotorL->setSpeed(O_speed + motor_speed_change);
  myMotorR->setSpeed(O_speed - motor_speed_change);
  Serial.print("Motor Speed");
  Serial.println(motor_speed_change);
  delay(250);
  }
  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear = pulseIn(echoPinrear, HIGH);
  cmrear = (durationrear/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Distancerearbackward: ");
  Serial.println(cmrear);
}

//LDR code

//insert function for ReachOtherSide()
bool ReachOtherSide() {
  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear = pulseIn(echoPinrear, HIGH);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  // Convert the time into a distance
  cm = (durationrear/2) / 29.1; 
  if (cm>170) {
    return true;
  }
  else {
    return false;
  }
}


// function for AllStop()
void AllStop() {
 myMotorL->run(RELEASE); // turns off left motor
 myMotorR->run(RELEASE); // turns off right motor
 Serial.println("Stop!");
}

//insert function for WallCompleted()
bool WallCompleted() {
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  cm = (durationrear/2) / 29.1; 
  if (cm > 183) {
    return true;
  }
  else {
    return false;
  }
}

//insert function for CarStopped()
bool CarStopped() {
  //measure distance
  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear = pulseIn(echoPinrear, HIGH);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  // Convert the time into a distance
  cm = (durationrear/2) / 29.1;    
  int distance = cm;
  delay(500);
  //measure distance2
   digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear = pulseIn(echoPinrear, HIGH);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  // Convert the time into a distance
  cm = (durationrear/2) / 29.1;    
  int distance2 = cm ;
  if (distance==distance2) {
    return true;
  }
  else {
    return false;
  }
 }

//insert function for OutofZone()
bool OutofZone() {
  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear = pulseIn(echoPinrear, HIGH);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  // Convert the time into a distance
  cm = (durationrear/2) / 29.1; 
  if (cm<21) {
    return true;
  }
  else {
    return false;
  }
}

//insert function for MoveToRight(step_size)
void RHReverse90() {
myMotorR->run(FORWARD);
}
void RHForward90() {
myMotorR->run(BACKWARD);
}

// function for AllReverse()?
void AllReverse() {
  //insert code
}
void MoveToRight(int stepsize){
  if (stepsize == 20){
    RHReverse90();
    delay(2100);
    AllStop();
    delay(1500);
    AllReverse();
    delay(900);
    AllStop();
    delay(1500);
    RHForward90();
    delay(2100);
    AllStop();
    delay(2500);
    }
  if (stepsize == 10){
    RHReverse90();
    delay(2100);
    AllStop();
    delay(1500);
// This should be tested or asked from Jay in order to have an exact movement of 10cm--------------------------------------------------------------------------------
    ReverseOut();
    delay(600);
    AllStop();
    delay(1500);
    RHForward90();
    delay(2100);
    AllStop();
    delay(2500);
    }
  }
  
int step_size;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  //Serial.println("Algorithm Test for Motors Only!");
  AFMS.begin();  // create with the default frequency 1.6KHz
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(trigPinrear, OUTPUT);
  pinMode(echoPinrear, INPUT);
  O_speed = 150;
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  //0_distance is the starting distance detected by the leftsensor
  O_distance = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Original Side Distance: ");
  Serial.println(O_distance);
// added new lines here ti give an initial reading of cmrear to kickstart the loop
  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear = pulseIn(echoPinrear, HIGH);
  cmrear = (durationrear/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Distancerear: ");
  Serial.println(cmrear);
}
String colour;
void loop() {
  // put your main code here, to run repeatedly:
  step_size = 20;
  if (ReachOtherSide()!=true){
        //write a function for moving forwards (at a steady speed)
         SteadyForward();
         colour = IdentifyColour1();
         if (IdentifyColour1() == "yellow" || IdentifyColour1() == "red"){
            // we could combine the following two if statements together since either way, we need to stop the car and record the coordinates----------------------------------------------
            //stop the car
            AllStop();
            delay(1000);
         }
         if (CarStopped()==true) {
            ReverseOut(); 
         }
         if (WallCompleted() == false && OutOfZone()== true) {
            //write a function that tells the robot to move to the right by step_size size "step" (unless this is implemented in the reversing function)
            MoveToRight(step_size);
          }
}
