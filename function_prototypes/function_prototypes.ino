//global variables
float step_size = 20;

int lowred = 350;
int upred = 450;
int lowyellow = 150;
int upyellow = 250;
String colour;

int redLed = 22;
int yellowLed = 52;
int LSensor = 0;
int CSensor = 1;
int RSensor = 2;

// check the exact pin number for these two ultrasonic sensors------------------------

int trigPinRear = 50
int echoPinRear = 48
int trigPinLeft = 46
int echoPinLeft = 45


int walls_completed = 0
// function prototypes

//function for going to initial start position from start box
void GoToStartPosition(void);

//function for moving forwards (at a steady speed)
void SteadyForward(void);

//function to check if the robot reaches the other side of the zone
bool ReachOtherSide(void);

//function to identify the colour of the mine detected by lefthand sensor
String IdentifyLColour(void);

//function to identify the colour of the mine detected by central sensor
String IdentifyCColour(void);

//function to identify the colour of the mine detected by righthand sensor
String IdentifyRColour(void);

//function to drive the car to the yellow mine depending on the remaining distance-----------there should be no such parameter once the sensors are fixed onto the robot and the remaining distance should be a fixed value 
void DriveToYellowMine(int dis);

//function to close the gate
void CloseGate(void);

//function to reverse the car our of the search zone 
void ReverseOut(void);

//function to open the gates to release the yellow mine outside of the search zone - make this function check that the mine would be safely out of the search zone when the mine is deposited
void OpenGate(void);

//function to record the coordinates of the mine detected by the sensor, it should be printed in the smae line as the colour detected or one line below it
float Coord(void);

//function to go to the start position of a new wall
void StartNewWall(void);

//write a function that tells the robot to move to the right by step_size size "step" (unless this is implemented in the reversing function)
void MoveToRight(float step_size);

//write a function that tells you if you have completed that wall
bool WallCompleted(void);

//write a function that tells the robot to return to the start box once the algorith has run
void ReturnToStartBox(void);
