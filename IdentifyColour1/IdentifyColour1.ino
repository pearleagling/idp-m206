#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
//motor declarations
double delta_t, motor_speed_change, heading, Kp, error;
static double O_distance, O_speed;
int trigPin = 11;    // Trigger
int echoPin = 12;    // Echo
long duration, cm, inches;

//colour declarations
double BaseReading;
double writetime, n, sumvalues, a, Y_bottom, Y_top, R_bottom, R_top;
int LDRPin1 = A0; // write A0 or something - analog pin for left most LDR
int LDRPin2 = A1;
int LDRPin3 = A2;
int LDRPin4 = A3;
int LDRPin5 = A4;


//not sure what this is for
int runattempt;

//motor stuff
// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

// Making two motor objects for left and right motors, connected to M1 nnd M2 respectively
Adafruit_DCMotor *myMotorL = AFMS.getMotor(2);
Adafruit_DCMotor *myMotorR = AFMS.getMotor(1);

void setup() {
  Serial.begin(9600);
  Serial.println("Algorithm Test for Motors Only!");
  AFMS.begin();  // create with the default frequency 1.6KHz
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  O_speed = 150;
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  O_distance = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  //Serial.print("Original Distance: ");
  //Serial.println(O_distance);
  
 }

//sorting function to sort the array to find largest and smallest values
int sort_desc(const void *cmp1, const void *cmp2)
  {
  // Need to cast the void * to int *
  int a = *((int *)cmp1);
  int b = *((int *)cmp2);
  // The comparison
  return a > b ? -1 : (a < b ? 1 : 0);
  // A simpler, probably faster way:
  //return b - a;
  }

 //makes an array of readings from LDR sensor
int mine_colour(int pin) {
  
  writetime = 250;
  n = 5;
  int colourvalues[250] = { };
  
  for (int i=0; i < writetime; i++){
    int colourvalue = analogRead(pin);
    colourvalues[i] = colourvalue;
    delay(2);
  }

  // Number of items in the array
  int arr_length = sizeof(colourvalues) / sizeof(colourvalues[0]);
  // qsort - last parameter is a function pointer to the sort function
  qsort(colourvalues, arr_length, sizeof(colourvalues[0]), sort_desc);
  // lt is now sorted
  
  sumvalues = colourvalues[0] + colourvalues[1] + colourvalues[2] + colourvalues[3] + colourvalues[4];
  
  int final_value = sumvalues/5;
  return final_value;
 }
 
String mine_detected() {
 
  BaseReading = 850; //700;
  int sensorValue1 = analogRead(LDRPin1);
  int sensorValue2 = analogRead(LDRPin2);
  int sensorValue3 = analogRead(LDRPin3);
  int sensorValue4 = analogRead(LDRPin4);
  int sensorValue5 = analogRead(LDRPin5);
  if (sensorValue1 or sensorValue2 or sensorValue3 or sensorValue4 or sensorValue5 > BaseReading) {
    myMotorL->setSpeed(30);
    myMotorR->setSpeed(30);
    myMotorL->run(BACKWARD); 
    myMotorR->run(BACKWARD);
  return IdentifyColour();
  }
  else{
    return "no mine";
  }
}

String IdentifyColour() {
   Y_bottom = 950;
   Y_top = 1100;
   R_bottom = 900;
   R_top = 950;
   int a = mine_colour(LDRPin1);
   int b = mine_colour(LDRPin2);
   int c = mine_colour(LDRPin3);
   int d = mine_colour(LDRPin4);
   int e = mine_colour(LDRPin5);
   delay(500);
   
   if (Y_bottom < a && a < Y_top) {
      return "yellow1";
   }
   else if (R_bottom < a && a < R_top) {
      return "red1";
   }
   if (Y_bottom < b && b < Y_top) {
      return "yellow2";
   }
   else if (R_bottom < b && b < R_top) {
      return "red2";
   }
   if (Y_bottom < c && c < Y_top) {
      return "yellow3";
   }
   else if (R_bottom < c && c < R_top) {
      return "red3";
   }
   if (Y_bottom < d && d < Y_top) {
      return "yellow4";
   }
   else if (R_bottom < d && d < R_top) {
      return "red4";
   }
   if (Y_bottom < e && e < Y_top) {
      return "yellow5";
   }
   else if (R_bottom < e && e < R_top) {
      return "red5";
   }
  }
  
 


 
 void drive(int timer){
  String colour;

  for (int i=0; i < timer; i++){
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  //Serial.print("Distance: ");
  //Serial.println(cm);

//P Control
  Kp = 3;
  error = cm - O_distance;
  motor_speed_change = Kp * error;

  colour = mine_detected();

  Serial.println(colour);

  myMotorL->setSpeed(O_speed + motor_speed_change);
  myMotorR->setSpeed(O_speed - motor_speed_change);
  myMotorL->run(BACKWARD); 
  myMotorR->run(BACKWARD); 

  //Serial.print("Motor Speed");
  //Serial.println(motor_speed_change);
  delay(250);
  }
}

void AllStop() {
 myMotorL->run(RELEASE); // turns off left motor
 myMotorR->run(RELEASE); // turns off right motor
 Serial.println("Stop!");
}



void loop(){

 myMotorL->setSpeed(O_speed);
 myMotorR->setSpeed(O_speed);

 drive(100);
 AllStop();
 delay(10000);


}
