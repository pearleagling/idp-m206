#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

double delta_t, motor_speed_change, heading, Kp, error, O_distance, O_speed;
int trigPin = 11;    // Trigger
int echoPin = 12;    // Echo
long duration, cm, inches;

int runattempt;

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

// Making two motor objects for left and right motors, connected to M1 nnd M2 respectively
Adafruit_DCMotor *myMotorL = AFMS.getMotor(2);
Adafruit_DCMotor *myMotorR = AFMS.getMotor(1);

void setup(void) 
{
  Serial.begin(9600);
  Serial.println("Ultrasound test!");
  AFMS.begin();  // create with the default frequency 1.6KHz
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  O_speed = 150;
  myMotorL->setSpeed(O_speed);
  myMotorR->setSpeed(O_speed);
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW); 

  
  LHForward90();
  delay(2000); 
}

void xdistance(){
   // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  O_distance = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Original Distance: ");
  Serial.println(O_distance);
}

void forward(int timer){

  for (int i=0; i < timer; i++){
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Distance: ");
  Serial.println(cm);

//P Control
  Kp = 3;
  error = cm - O_distance;
  motor_speed_change = Kp * error;

  myMotorL->run(BACKWARD); 
  myMotorR->run(BACKWARD); 

  myMotorL->setSpeed(O_speed + motor_speed_change);
  myMotorR->setSpeed(O_speed - motor_speed_change);

Serial.print("Motor Speed");
  Serial.println(motor_speed_change);
  delay(250);
  }
}

void backward(int timer){

  for (int i=0; i < timer; i++){
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(echoPin, INPUT);
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Distance: ");
  Serial.println(cm);

//P Control
  Kp = 3;
  error = cm - O_distance;
  motor_speed_change = Kp * error;

  myMotorL->run(FORWARD); 
  myMotorR->run(FORWARD); 

  myMotorL->setSpeed(O_speed + motor_speed_change);
  myMotorR->setSpeed(O_speed - motor_speed_change);

Serial.print("Motor Speed");
  Serial.println(motor_speed_change);
  delay(250);
  }
}

void AllStop() {
 myMotorL->run(RELEASE); // turns off left motor
 myMotorR->run(RELEASE); // turns off right motor
 Serial.println("Stop!");
}

//Rotate90A(): Makes robot rotate 90 degrees anticlockwise
void Rotate90A() {
myMotorL->run(FORWARD);
myMotorR->run(BACKWARD);
Serial.println("Rotating 90 degrees anticlockwise");
}

//Rotate90C(): Makes robot rotate 90 degrees clockwise
void Rotate90C() {
myMotorL->run(BACKWARD);
myMotorR->run(FORWARD);
Serial.println("Rotating 90 degrees clockwise");
}

void RHReverse90() {
myMotorL->run(FORWARD);
}

void LHReverse90() {
myMotorR->run(FORWARD);
}

void LHForward90() {
myMotorR->run(BACKWARD);
}

void RHForward90() {
myMotorL->run(BACKWARD);
}

//AllForward(): Makes robot move forwards
void AllForward() {
myMotorL->run(BACKWARD); 
myMotorR->run(BACKWARD); 
Serial.println("Going forwards");
}

//AllReverse(): Makes robot reverse backwards
void AllReverse() {
myMotorL->run(FORWARD); 
myMotorR->run(FORWARD); 
Serial.println("Going backwards");
}

void loop() { if (runattempt == 0) {


xdistance();

forward(16);

AllStop();
delay(1000);

backward(16);

RHReverse90();
delay(2000);

AllReverse();
delay(1000);

RHForward90();
delay(2000);

O_distance += 20;

forward(16);

AllStop();
delay(1000);

backward(16);
delay(1000);

runattempt +=1;
}
}
