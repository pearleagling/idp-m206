#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
int runattempt;

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// Making two motor objects for left and right motors, connected to M1 nnd M2 respectively
Adafruit_DCMotor *myMotorL = AFMS.getMotor(2);
Adafruit_DCMotor *myMotorR = AFMS.getMotor(1);

void setup() {
  Serial.begin(9600); // set up Serial library at 9600 bps
  Serial.println("Algorithm Test for Motors Only!");
  AFMS.begin();  // create with the default frequency 1.6KHz

  myMotorL->setSpeed(150);
  myMotorR->setSpeed(150);
}

//AllStop(): Makes robot stop
void AllStop() {
  myMotorL->run(RELEASE); // turns off left motor
  myMotorR->run(RELEASE); // turns off right motor 
  Serial.println("Stop!");
}

//AllForward(): Makes robot move forwards
void AllForward() {
  myMotorL->run(FORWARD); // turns off left motor
  myMotorR->run(FORWARD); // turns off right motor 
  Serial.println("Going forwards");
}

//AllReverse(): Makes robot move backwards
void AllReverse() {
  myMotorL->run(BACKWARD); // turns off left motor
  myMotorR->run(BACKWARD); // turns off right motor 
  Serial.println("Going backwards");
}

//Rotate90A(): Makes robot rotate 90 degrees anticlockwise
void Rotate90A() {
  myMotorL->run(BACKWARD);
  myMotorR->run(FORWARD);
  Serial.println("Rotating 90 degrees anticlockwise");
}

//Rotate90C(): Makes robot rotate 90 degrees clockwise
void Rotate90C() {
  myMotorL->run(FORWARD);
  myMotorR->run(BACKWARD);
  Serial.println("Rotating 90 degrees clockwise");
}

void loop() { if (runattempt == 0) {

uint8_t i;

Serial.print("Start test!");

Rotate90A();
delay(1300);

AllStop();
delay(2000);

Serial.print("Start loop!");

for (i=0; i<5; i++) {

AllForward();
delay(5000);

AllStop();
delay(2000);

AllReverse();
delay(5000);

AllReverse();
delay(1000);

AllStop();
delay(2000);

Rotate90A();
delay(1300);

AllStop();
delay(2000);

AllReverse();
delay(1000);

Rotate90C();
delay(1300);

AllForward();
delay(1000);

AllStop();
delay(2000);

}
runattempt +=1;
}


}
