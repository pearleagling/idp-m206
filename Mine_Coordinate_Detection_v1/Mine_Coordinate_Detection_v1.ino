#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

double delta_t, motor_speed_change, heading, Kp, error;
static double O_distanceleft, O_distance, O_distancerear, O_speed;

int trigPinleft = 11;    // Triggerleft
int echoPinleft = 12;    // Echoleft
int trigPinrear = 8;    // Triggerrear
int echoPinrear = 9;    // Echorear
long duration, durationrear, durationleft, durationrear1, durationleft1, C_distanceleft, C_distancerear;

void setup(void) 
{
  
  Serial.begin(9600);
  Serial.println("Mine Coordinate Detection Test!");
  pinMode(trigPinleft, OUTPUT);
  pinMode(echoPinleft, INPUT);
  pinMode(trigPinrear, OUTPUT);
  pinMode(echoPinrear, INPUT);
  O_speed = 150;
  
  digitalWrite(trigPinleft, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinleft, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinleft, LOW);
 
  durationleft = pulseIn(echoPinleft, HIGH);
 
  // Convert the time into a distance
  //O_distance is the starting distance detected by the leftsensor
  O_distanceleft = (durationleft/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Original Side Distance: ");
  Serial.println(O_distance);
  
// added new lines here ti give an initial reading of the rear distance to kickstart the loop
  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  
  durationrear = pulseIn(echoPinrear, HIGH);
  O_distancerear = (durationrear/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Original Rear Distance: ");
  Serial.println(O_distancerear);
}

String IdentifyColour1() {
  return "red";
}

String IdentifyColour2() {
  return "green";
}

String IdentifyColour3() {
  return "Black";
}

String IdentifyColour4() {
  return "Orange";
}

String IdentifyColour5() {
  return "Brown";
}

void readcoords(int walls) {
  
  digitalWrite(trigPinleft, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinleft, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinleft, LOW);
  durationleft1 = pulseIn(echoPinleft, HIGH);
  C_distanceleft = (durationleft1/2) / 29.1;
  Serial.print("Current Side Distance: ");
  Serial.println(C_distanceleft);

  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear1 = pulseIn(echoPinrear, HIGH);
  C_distancerear= (durationrear1/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Current Rear Distance: ");
  Serial.println(C_distancerear);

int redminecoord[2];
int yellowminecoord[2];
int minecoord[2];
int xcoord, ycoord;

//Need to add offset from ultrasound sensor to LDR for each LDR
int offset;
int L = 200; //check this value

if (IdentifyColour1() == "red" || IdentifyColour1() == "yellow") {
  offset = 0;
}

if (IdentifyColour2()=="red" || IdentifyColour2() == "yellow") {
  offset = 5.5;
}

if (IdentifyColour3()=="red" || IdentifyColour3() == "yellow") {
  offset = 11;
}

if (IdentifyColour4()=="red" || IdentifyColour4() == "yellow") {
  offset = 16.5;
}

if (IdentifyColour5()=="red" || IdentifyColour5() == "yellow") {
  offset = 22;
}

minecoord[0] =  C_distanceleft + offset;
minecoord[1] = C_distancerear + 30;
Serial.print("Coordinates of mine relative to robot: ");
Serial.println(minecoord[0], minecoord[1]);

if (walls == 1) {
  
xcoord = minecoord[0];
ycoord = minecoord[1];

if (IdentifyColour1() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour1() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR1:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour2() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR1: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour2() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR2:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour3() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR3: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour3() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR3:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour4() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR4: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour4() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR4:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour5() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR5: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour5() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR5:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

}

if (walls ==2) {
  xcoord = L-minecoord[1];
  ycoord = minecoord[0];
  
  
if (IdentifyColour1() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour1() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR1:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour2() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR1: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour2() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR2:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour3() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR3: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour3() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR3:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour4() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR4: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour4() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR4:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour5() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR5: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour5() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR5:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

}

if (walls == 4) {
  xcoord = minecoord[1];
  ycoord = L - minecoord[0];
  
  
if (IdentifyColour1() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour1() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR1:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour2() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR1: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour2() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR2:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour3() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR3: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour3() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR3:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour4() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR4: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour4() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR4:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}

if (IdentifyColour5() = "red") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.print("Coordinates of red mine detected on LDR5: ");
  Serial.println(redminecoord[0], redminecoord[1]);
}

if (IdentifyColour5() = "yellow") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR5:");
  Serial.println(yellowminecoord[0], yellowminecoord[1]);  
}
  
}  

}
