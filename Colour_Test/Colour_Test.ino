
double BaseReading;
double writetime, n, sumvalues, a,Y_bottom, Y_top, R_bottom, R_top ;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

int sort_desc(const void *cmp1, const void *cmp2)
  {
  // Need to cast the void * to int *
  int a = *((int *)cmp1);
  int b = *((int *)cmp2);
  // The comparison
  return a > b ? -1 : (a < b ? 1 : 0);
  // A simpler, probably faster way:
  //return b - a;
  }

int mine_colour() {
  
  writetime = 250;
  n = 5;
  int colourvalues[250] = { };
  
  for (int i=0; i < writetime; i++){
    int colourvalue = analogRead(A0);
    colourvalues[i] = colourvalue;
    delay(2);
  }

  // Number of items in the array
  int arr_length = sizeof(colourvalues) / sizeof(colourvalues[0]);
  // qsort - last parameter is a function pointer to the sort function
  qsort(colourvalues, arr_length, sizeof(colourvalues[0]), sort_desc);
  // lt is now sorted

  sumvalues = colourvalues[0] + colourvalues[1] + colourvalues[2] + colourvalues[3] + colourvalues[4];
  
  int final_value = sumvalues/5;
  return final_value;
  
}

void loop() {
  // put your main code here, to run repeatedly:
  Y_bottom = 900;
  Y_top = 1100;
  R_bottom = 780;
  R_top = 870;
  BaseReading = 700;
  int sensorValue = analogRead(A0);
  Serial.println(sensorValue);
  if (sensorValue > BaseReading) {
  int a = mine_colour();
  Serial.println(a);
  if (Y_bottom < a && a < Y_top){
    Serial.println("yellow"); 
  }
  else if (R_bottom < a && a < R_top){
    Serial.println("Red"); 
  }
  }
  delay(1000);

}
