
void setup() {
  // put your setup code here, to run once:
  //also write code to set up arduino board?
  //write a function for going to initial start position from start box
  GoToStartPosition();
}

void loop() {
  // put your main code here, to run repeatedly:
  if (walls_completed < 4){
    //write a float/integer for step size
    step_size = 20
    if (ReachOtherSide()!=true){
        //write a function for moving forwards (at a steady speed)
         SteadyForward();
         if (IdentifyLColour() == "yellow" || IdentifyLColour() == "red"){
            // we could combine the following two if statements together since either way, we need to stop the car and record the coordinates----------------------------------------------
            //stop the car
            CarStop();
            if IdentifyLColour() == "yellow"{
            //set step_size
            //step_size = 20;
            //record the coordinates of that mine detected
            Coord();
            }
            if IdentifyLColour() == "red"{
              //stop the car
              //CarStop();
              //set step_size
              //step_size = 20;
              //record the coordinates of that mine detected
              Coord();
              }
              
              //check with the central sensor 
             if (IdentifyCColour() == "yellow"){
              //CarStop();
              Coord();
              //function to drive the robot forward by an appropraite distance once a yellow mine is detected
              DriveToYellowMine();
              //function to close the gate to collect the yellow mine
              GateClose();
              //step_size = 20;
              }
              
             if (IdentifyCColour() == "red") {
              //CarStop();
              Coord();
              //step_size = 20;
              }

              // check with the right sensor about mine detection
             if (IdentifyRColour() == "yellow"){
              //CarStop();
              Coord();
              step_size = 10;
              //write a function which tells the car to reverse out of the search zone, ready to either deposit a mine or line up at its next position
              //ReverseOut();  
              }
             if (IdentifyRColour() == "red"){
              //CarStop();
              Coord();
              //step_size = 20;
              //write a function which tells the car to reverse out of the search zone, ready to either deposit a mine or line up at its next position
              //ReverseOut();  
              }
          }
        else if (IdentifyCColour() == "yellow" || IdentifyCColour() == "red"){
            //check with the central sensor 
            CarStop();
             if (IdentifyCColour() == "yellow"){
              //CarStop();
              Coord();
              //function to drive the robot forward by an appropraite distance once a yellow mine is detected
              DriveToYellowMine();
              //function to close the gate to collect the yellow mine
              GateClose();
              //step_size = 20;
              }
              
             if (IdentifyCColour() == "red") {
              //CarStop();
              Coord();
              //step_size = 20;
              }

              // check with the right sensor about mine detection
             if (IdentifyRColour() == "yellow"){
              //CarStop();
              Coord();
              step_size = 10;
              //write a function which tells the car to reverse out of the search zone, ready to either deposit a mine or line up at its next position
              //ReverseOut();  
              }
             if (IdentifyRColour() == "red"){
              //CarStop();
              Coord();
              //step_size = 20;
              //write a function which tells the car to reverse out of the search zone, ready to either deposit a mine or line up at its next position
              //ReverseOut();  
              }
            }

          else if (IdentifyRColour() == "yellow" || IdentifyRColour() == "red"){
            // check with the right sensor about mine detection
            CarStop();
             if (IdentifyRColour() == "yellow"){
              //CarStop();
              Coord();
              step_size = 10;
              //write a function which tells the car to reverse out of the search zone, ready to either deposit a mine or line up at its next position
              //ReverseOut();  
              }
             if (IdentifyRColour() == "red"){
              //CarStop();
              Coord();
              //step_size = 20;
              //write a function which tells the car to reverse out of the search zone, ready to either deposit a mine or line up at its next position
              //ReverseOut();  
              }
            } 
        //write a function which tells the car to reverse out of the search zone, ready to either deposit a mine or line up at its next position
        if (CarStopped()==true) {
          ReverseOut(); 
        }
        if (GatesClosed()==true) {
          DeopsitMine();
        }
        
        }
        if (ReachedOtherSide()==true){
          CarStop();
          ReverseOut();
        }
        //write a function that tells you if you have completed that wall
        if (WallCompleted() == true && OutOfZone() == true) {
          runattempt+=1;
          StartNewWall();
        }
        else if (WallCompleted() == false && OutOfZone()== true) {
          //write a function that tells the robot to move to the right by step_size size "step" (unless this is implemented in the reversing function)
          MoveToRight(step_size);
        }
      }      

  if (runattempt==4 && OutOfZone() == true){
    //write a function that tells the robot to return to the start box once the algorith has run
    ReturnToStartBox();
    runattempt+=1
    }
}
