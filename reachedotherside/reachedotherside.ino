// decalre global variables, THESE SHOULD APPEAR AT THE VERY BEGINNING OF THE CODE----------------------------------
int lowred = 350;
int upred = 450;
int lowyellow = 150;
int upyellow = 250;
String colour;

int redLed = 22;
int yellowLed = 52;
int LSensor = 0;
int CSensor = 1;
int RSensor = 2;

/*HC-SR04 Ping distance sensor]
VCC to arduino 5v GND to arduino GND
Echo to Arduino pin 13 Trig to Arduino pin 12
Red POS to Arduino pin 11
Green POS to Arduino pin 10
560 ohm resistor to both LED NEG and GRD power rail
*/


// set up the trig pins and echo pins for both rear and left ultrasonic sensors
int trigPinRear = 50;
int echoPinRear = 48;
int trigPinLeft = 46;
int echoPinLeft = 45;
long DurationRear, DurationLeft, cmRear, cmLeft;

void setup() {
  //Serial Port begin
  Serial.begin (9600);
  //Define inputs and outputs
  pinMode(trigPinRear, OUTPUT);
  pinMode(trigPinLeft, OUTPUT);
  pinMode(echoPinRear, INPUT);
  pinMode(echoPinLeft, INPUT);
}
 
void loop() {
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(trigPinRear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinRear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinRear, LOW);

  digitalWrite(trigPinLeft, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinLeft, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinLeft, LOW);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  
  pinMode(echoPinRear, INPUT);
  pinMode(echoPinLeft, INPUT);

  DurationRear = pulseIn(echoPinRear, HIGH);
  DurationLeft = pulseIn(echoPinLeft, HIGH);

 
  // Convert the time into a distance
  cmRear = (DurationRear/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  cmLeft = (DurationLeft/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343

  Serial.print(cmRear);
  Serial.print("cm");
  Serial.println();
  
  delay(250);
}

// write a function to check id the other side of the area is reached
//check what variable name eletronics people use for the distance detected
bool ReachOtherSide() {
  if (cmRear >= 173){
    return true;
    }
  else{
    return false;
    }
}
