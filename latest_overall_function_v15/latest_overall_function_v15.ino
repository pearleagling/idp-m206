#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Servo.h>


Servo myservoleft;  // create servo object to control a servo
Servo myservoright;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int posright = 65;    // variable to store the right servo position
int posleft = 21;    // variable to store the left servo position
int runattempt;

static double O_distance, O_speed;
int trigPin = 11;    // Trigger
int echoPin = 12;    // Echo
int trigPinrear = 8;    // Triggerrear
int echoPinrear = 13;    // Echorear
int RedPin = 24;
int YellowPin = 22;
long duration, cm, inches, durationrear, durationleft1, durationrear1, cmrear;

// constants won't change. They're used here to set pin numbers:
const int buttonPinleft = 2;     // the number of the pushbutton pin
const int buttonPinright = 3;     // the number of the pushbutton pin

// variables will change:
int buttonStateleft = 0;         // variable for reading the pushbutton status
int buttonStateright = 0;         // variable for reading the pushbutton status
String colour = "no mine";
int walls_completed= 0;    // number of walls the robot has completed. It should reach 3 (0-3) by the end of the loop
int step_size=20;
int PassNum = 0;

//colour declarations
String col;
double BaseReading1, BaseReading2, BaseReading3, BaseReading4, BaseReading5;
double writetime, n, sumvalues, Y_bottom, Y_top, R_bottom, R_top, a, b, c, d, e;
int LDRPin1 = A1; // write A0 or something - analog pin for left most LDR
int LDRPin2 = A2;
int LDRPin3 = A3;
int LDRPin4 = A4;
int LDRPin5 = A5;

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

// Making two motor objects for left and right motors, connected to M1 nnd M2 respectively
Adafruit_DCMotor *myMotorL = AFMS.getMotor(2);
Adafruit_DCMotor *myMotorR = AFMS.getMotor(1);

 
 void setup() {

  delay(2000);

  // intitalize the motor
  Serial.begin(9600);
  Serial.println("Algorithm Test!");
  AFMS.begin();  // create with the default frequency 1.6KHz
  O_speed = 150;
  myMotorL->setSpeed(O_speed);
  myMotorR->setSpeed(O_speed);

  //also write code to set up arduino board?
  //write a function for going to initial start position from start box---------------------------------------------
 // GoToStartPosition();


  // initialize the ultrasonic sensors, giving the rear and side distance readings
  pinMode(24, OUTPUT); //red
  pinMode(22, OUTPUT); //yellow
    
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(trigPinrear, OUTPUT);
  pinMode(echoPinrear, INPUT);
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
 
  
  // duration is the time (in microseconds) from the sending of the ping to the reception of its echo off of an object.
  duration = pulseIn(echoPin, HIGH);
 
  // Convert the time into a distance
  //0_distance is the starting distance detected by the leftsensor
  O_distance = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Original Side Distance: ");
  Serial.println(O_distance);

// added new lines here to give an initial reading of cmrear to kickstart the loop
  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear = pulseIn(echoPinrear, HIGH);
  cmrear = (durationrear/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Distancerear: ");
  Serial.println(cmrear);

  // initialize the servors.
  myservoright.attach(10);  // attaches the right servo on pin 9 to the servo object
  myservoleft.attach(9);  // attaches the left servo on pin 9 to the servo object

  //open the gate once and leave it as open.
  // goes from 15 degrees to 70 degrees in step of 1 degree
  for (posright = 50; posright <= 120; posright += 1) { 
    myservoright.write(posright);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  
  for (posleft = 90; posleft >= 40; posleft -= 1) { // goes from 70 degrees to 25 degrees
    myservoleft.write(posleft);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  
  
  // initialize the pushbutton pin as an input:
  pinMode(buttonPinleft, INPUT);
  pinMode(buttonPinright, INPUT);

  // initialize the LED pin modes
  pinMode(RedPin,OUTPUT);
  pinMode(YellowPin,OUTPUT);

  delay(2000);
LHForward90();
delay(2400);
AllStop();
delay(1000);

while (TouchTheWall() ==false) {
  AllReverse();
}

AllStop();
delay(2000);

AllForward();
delay(1500);
 }
  //instruct the robot to move backwards and hit hte wall until it's perpendicular
 /* while (TouchTheWall()==false){
    AllReverse();
    }
*/




  // in the following sections, various functions are created and will be used in the final loop---------------------------------------------- 
  //-----------------------------------------------------------------------------------------------------------------------------

 // check if hte robot is touching the wall
 bool TouchTheWall(){
  buttonStateleft = digitalRead(buttonPinleft);
  buttonStateright = digitalRead(buttonPinright);

  if (buttonStateleft == HIGH && buttonStateright == HIGH) {
    Serial.print("touching the wall ");
    return true;
  }

  else{
    return false;
    }
  }

 
  
  // make the robot go to start position. Depeding on the start position of the robot-----------------------------
void GoToStartPosition(){
      RHReverse90();
    delay(2100);

    AllStop();
    delay(1500);
   while (TouchTheWall() ==false){
                 AllReverse();
                 }
  }

  // Boolean to check if the robot has reached the other end of the wall
bool ReachedOtherSide() {
  if (cmrear >= 173){
    return true;
    }
  else{
    return false;
    }
}


  // The robot moves steadily forward without any PID control------------------------------------------
  void SteadyForward(){
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  myMotorL->run(BACKWARD); 
  myMotorR->run(BACKWARD); 
  myMotorL->setSpeed(O_speed);
  myMotorR->setSpeed(O_speed);
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  
  Serial.print("SideDistance: ");
  Serial.println(cm);

  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear = pulseIn(echoPinrear, HIGH);
  cmrear = (durationrear/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Distancerearforward: ");
  Serial.println(cmrear);

  }

  

  // Robot moves backward steadily without PID control-------------------------------------------------
void BackwardDrive(){

  myMotorL->run(FORWARD); 
  myMotorR->run(FORWARD); 
  myMotorL->setSpeed(O_speed);
  myMotorR->setSpeed(O_speed);

  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);

  // Convert the time into a distance
  cm = (duration/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("SideDistance: ");
  Serial.println(cm);

  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear = pulseIn(echoPinrear, HIGH);
  cmrear = (durationrear/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Distancerear: ");
  Serial.println(cmrear);

  }

//AllReverse(): Makes robot reverse backwards
void AllReverse() {
myMotorL->run(FORWARD); 
myMotorR->run(FORWARD); 
Serial.println("Going backwards");
}


//Rotate90C(): Makes robot rotate 90 degrees clockwise
void Rotate90C() {
myMotorL->run(BACKWARD);
myMotorR->run(FORWARD);
Serial.println("Rotating 90 degrees clockwise");
}


void RHReverse90() {
myMotorR->run(FORWARD);
}


void RHForward90() {
myMotorR->run(BACKWARD);
}

void LHForward90() {
myMotorL -> run(BACKWARD);
}

  // instruct the car to stop
  // function for AllStop()
void AllStop() {
 myMotorL->run(RELEASE); // turns off left motor
 myMotorR->run(RELEASE); // turns off right motor
 Serial.println("Stop!");
}

//AllForward(): Makes robot move forwards
void AllForward() {
myMotorL->run(BACKWARD); 
myMotorR->run(BACKWARD); 
Serial.println("Going forwards");
}

void MoveToRight(int step_size){
  if (step_size == 20){
    
              RHReverse90();
               delay(2100);

               AllStop();
               delay(1500);

               OpenGate();
              delay(1500);

              AllReverse();
              delay(1500);

              AllStop();
              delay(1500);

              RHForward90();
              delay(2100);

              AllStop();
              delay(2500);
  }
    
  if (step_size == 10){
// This should be tested or asked from Jay in order to have an exact movement of 10cm--------------------------------------------------------------------------------
               RHReverse90();
               delay(2100);

               AllStop();
               delay(1500);

               OpenGate();
              delay(1500);

              AllReverse();
              delay(1500);

              AllStop();
              delay(1500);

              RHForward90();
              delay(2100);

              AllStop();
              delay(2500);
  }
  }


void CloseGate(){
  for (posright = 120; posright >= 50; posright -= 1) { // goes from 180 degrees to 0 degrees
    myservoright.write(posright);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (posleft = 40; posleft <= 90; posleft += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservoleft.write(posleft);              // tell servo to go to position in variable 'pos'
    delay(15);    
  }
  }

void OpenGate(){
   for (posright = 50; posright <= 120; posright += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservoright.write(posright);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  for (posleft = 90; posleft >= 40; posleft -= 1) { // goes from 180 degrees to 0 degrees
    myservoleft.write(posleft);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  }



  //LDR reads the value and prints the colour on the serial monitor------------------------------------------
/*
void mine_detected(){
  void
  }
*/

//sorting function to sort the array to find largest and smallest values
int sort_desc(const void *cmp1, const void *cmp2)
  {
  // Need to cast the void * to int *
  int a = *((int *)cmp1);
  int b = *((int *)cmp2);
  // The comparison
  return a > b ? -1 : (a < b ? 1 : 0);
  // A simpler, probably faster way:
  //return b - a;
  }
 //makes an array of readings from LDR sensor
int mine_colour(int pin) {
  
  writetime = 250;
  n = 5;
  int colourvalues[250] = { };
  
  for (int i=0; i < writetime; i++){
    int colourvalue = analogRead(pin);
    colourvalues[i] = colourvalue;
    delay(5);
  }
  // Number of items in the array
  int arr_length = sizeof(colourvalues) / sizeof(colourvalues[0]);
  // qsort - last parameter is a function pointer to the sort function
  qsort(colourvalues, arr_length, sizeof(colourvalues[0]), sort_desc);
  // lt is now sorted
  
  sumvalues = colourvalues[0] + colourvalues[1] + colourvalues[2] + colourvalues[3] + colourvalues[4];
  
  int final_value = sumvalues/5;
  return final_value;
 }


String mine_detected() {

//Need to calibrate base readings and value for red & yellow mines every time 
  BaseReading1 = 770; 
  BaseReading2 = 845;
  BaseReading3 = 770;
  BaseReading4 = 865;
  BaseReading5 = 840;
  
  int sensorValue1 = analogRead(LDRPin1);
  Serial.print(sensorValue1);
  
  int sensorValue2 = analogRead(LDRPin2);
  Serial.print(sensorValue2);
  
  int sensorValue3 = analogRead(LDRPin3);
  Serial.print(sensorValue3);
  
  int sensorValue4 = analogRead(LDRPin4);
  Serial.print(sensorValue4);
  
  int sensorValue5 = analogRead(LDRPin5);
  Serial.print(sensorValue5);
  
  if (sensorValue1 > BaseReading1) {
    Serial.print(sensorValue1);
    myMotorL->setSpeed(15);
    myMotorR->setSpeed(15);
    myMotorL->run(FORWARD); 
    myMotorR->run(FORWARD);
    delay(100);
    AllStop();
    delay(1000);
    myMotorL->run(BACKWARD); 
    myMotorR->run(BACKWARD);
    Serial.print("Mine detected on LDR1");
    
  return IdentifyColour1();
  //delay(2000);
  }
  
  else if (sensorValue2 > BaseReading2) {
    Serial.print(sensorValue2);
    myMotorL->setSpeed(15);
    myMotorR->setSpeed(15);
    myMotorL->run(FORWARD); 
    myMotorR->run(FORWARD);
    delay(100);
    AllStop();
    delay(1000);
    myMotorL->run(BACKWARD); 
    myMotorR->run(BACKWARD);
    Serial.print("Mine detected on LDR2");
  return IdentifyColour2();
  delay(2000);
  }

  else if (sensorValue3 > BaseReading3) {
    Serial.print(sensorValue3);
    myMotorL->setSpeed(15);
    myMotorR->setSpeed(15);
    myMotorL->run(FORWARD); 
    myMotorR->run(FORWARD);
    delay(100);
    AllStop();
    delay(1000);
    myMotorL->run(BACKWARD); 
    myMotorR->run(BACKWARD);
    Serial.print("Mine detected on LDR3");
  return IdentifyColour3();
  delay(2000);
  }

   else if (sensorValue4 > BaseReading4) {
    Serial.print(sensorValue4);
    myMotorL->setSpeed(15);
    myMotorR->setSpeed(15);
    myMotorL->run(FORWARD); 
    myMotorR->run(FORWARD);
    delay(100);
    AllStop();
    delay(1000);
    myMotorL->run(BACKWARD); 
    myMotorR->run(BACKWARD);
    Serial.print("Mine detected on LDR4");
  return IdentifyColour4();
  delay(2000);
  }

   else if (sensorValue5 > BaseReading5) {
    Serial.print(sensorValue5);
    myMotorL->setSpeed(15);
    myMotorR->setSpeed(15);
    myMotorL->run(FORWARD); 
    myMotorR->run(FORWARD);
    delay(100);
    AllStop();
    delay(1000);
    myMotorL->run(BACKWARD); 
    myMotorR->run(BACKWARD);
    Serial.print("Mine detected on LDR5");
  return IdentifyColour5();
  //delay(2000);
  }
      
  else{
    return "";   
  }
    myMotorL->setSpeed(150);
    myMotorR->setSpeed(150);  
}


String IdentifyColour1() {
   Y_bottom = 900;
   Y_top = 1050;
   R_bottom = 800;
   R_top = 900;
   int a = mine_colour(LDRPin1);
   delay(250);

   if (Y_bottom < a && a < Y_top) {
      return "yellow1";
   }
   else if (R_bottom < a && a < R_top) {
      return "red1";
   }
   else {
    return "nothing";
   }
}

String IdentifyColour2() {
   Y_bottom = 930;
   Y_top = 1050;
   R_bottom = 890;
   R_top = 930;
   int b = mine_colour(LDRPin2);
   delay(250);

   if (Y_bottom < b && b < Y_top) {
      return "yellow2";
   }
   else if (R_bottom < b && b < R_top) {
      return "red2";
   }
   else {
    return "nothing";
   }
}

String IdentifyColour3() {
   Y_bottom = 875;
   Y_top = 1000;
   R_bottom = 840;
   R_top = 875;
   int c = mine_colour(LDRPin3);
   delay(250);

   if (Y_bottom < c && c < Y_top) {
      return "yellow3";
   }
   else if (R_bottom < c && c < R_top) {
      return "red3";
   }
   else {
    return "nothing";
   }
}

String IdentifyColour4() {
   Y_bottom = 915;
   Y_top = 1000;
   R_bottom = 900;
   R_top = 915;
   int d = mine_colour(LDRPin4);
   delay(250);

   if (Y_bottom < d && d < Y_top) {
      return "yellow4";
   }
   else if (R_bottom < d && d < R_top) {
      return "red4";
   }
    else {
    return "nothing";
   }
}

String IdentifyColour5() {
   Y_bottom = 895;
   Y_top = 1000;
   R_bottom = 860;
   R_top = 895;
   int e = mine_colour(LDRPin5);
   delay(250);

   if (Y_bottom < e && e < Y_top) {
      return "yellow5";
   }
   else if (R_bottom < e && e < R_top) {
      return "red5";
   }

   else {
    return "nothing";
   }
}






bool MineDetected(){
  if (col == "yellow1" || col == "yellow2" || col == "yellow3" || col == "yellow4" || col == "yellow5" || col == "red1" || col == "red2" || col == "red3" || col == "red4" || col == "red5"){
    return true;
    }
  else{
    return false;
    }
  }



  // boolean checks if the robot detects the mine by any of the sensors
bool MineDetectedLeft(){
  if (col == "yellow1" || col == "red1"){
    return true;
    }
   else{
    return false;
    }
  }

bool MineDetectedCenter(){
  if (col == "yellow2" || col == "yellow3" || col == "red2"  || col == "red3" || col == "yellow4" || col == "red4"){
    return true;
    }
   else{
    return false;
    }
  }

bool MineDetectedRight(){
  if (col == "yellow5" || col == "red5"){
    return true;
    }
   else{
    return false;
    }
  }
  

  // once the yellow mine is detected, the robot will move to the yellow mine-----------------------------
void DriveToYellowMine(){
  myMotorL->setSpeed(30);
  myMotorR->setSpeed(30);
  AllForward();
  delay(700);
  }

void readcoords(int walls_completed, String col) {
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  durationleft1 = pulseIn(echoPin, HIGH);
  cm = (durationleft1/2) / 29.1;
  Serial.print("Current Side Distance: ");
  Serial.println(cm);

  digitalWrite(trigPinrear, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinrear, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPinrear, LOW);
  durationrear1 = pulseIn(echoPinrear, HIGH);
  cmrear= (durationrear1/2) / 29.1;     // Divide by 29.1 or multiply by 0.0343
  Serial.print("Current Rear Distance: ");
  Serial.println(cmrear);

int redminecoord[2]; //might be incorrect type
int yellowminecoord[2];
int minecoord[2];
int xcoord, ycoord;

//Need to add offset from ultrasound sensor to LDR for each LDR
int offset;
int L = 200; //check this value

if (col == "red1" || col == "yellow1") {
  offset = 0;
}

else if (col=="red2" || col == "yellow2") {
  offset = 5.5;
}

else if (col=="red3" || col == "yellow3") {
  offset = 11;
}

else if (col=="red4" || col == "yellow4") {
  offset = 16.5;
}

else if (col=="red5" || col == "yellow5") {
  offset = 22;
}

else {
  return;
}

minecoord[0] =  cm + offset; 
minecoord[1] = cmrear + 30;
Serial.println("Coordinates of mine relative to robot: ");
Serial.print("Side coordinate to LDR: ");
Serial.println(minecoord[0]);
Serial.print("Rear coordinate to LDR: ");
Serial.println(minecoord[1]);

if (walls_completed == 0) {
  
xcoord = minecoord[0];
ycoord = minecoord[1];
Serial.println(xcoord);
Serial.println(ycoord);

Serial.print("------------------------------------------------");
Serial.println (col);


if (col == "red1") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("it goes into col==red1 loop");
  Serial.println("Coordinates of red mine detected on LDR1: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow1") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR1:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red2") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR2: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow2") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR2:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red3") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR3: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow3") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR3:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red4") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR4: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow4") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR4:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red5") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR5: ");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow5") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR5:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
  }
}

if (walls_completed == 1) {
  xcoord = L-minecoord[1];
  ycoord = minecoord[0];

if (col == "red1") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR1: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow1") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR1:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red2") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR2: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow2") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR2:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red3") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR3: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow3") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR3:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red4") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR4: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow4") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR4:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red5") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR5: ");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow5") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR5:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
  }
}
  
if (walls_completed == 2) {
  xcoord = L-minecoord[0];
  ycoord = L-minecoord[1];
  
 
if (col == "red1") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR1: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow1") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR1:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red2") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR2: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow2") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR2:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red3") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR3: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow3") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR3:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red4") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR4: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow4") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR4:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red5") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR5: ");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow5") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR5:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
  }
}

if (walls_completed == 3) {
  xcoord = minecoord[1];
  ycoord = L - minecoord[0];

if (col == "red1") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR1: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow1") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR1:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red2") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR2: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow2") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR2:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red3") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR3: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow3") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR3:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red4") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR4: ");
  Serial.print("x-coordinate: ");
  Serial.println(redminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(redminecoord[1]);
  Serial.println("------------------");
}

else if (col =="yellow4") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.println("Coordinates of yellow mine detected on LDR4:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "red5") {
  redminecoord[0] = xcoord;
  redminecoord[1] = ycoord;
  Serial.println("Coordinates of red mine detected on LDR5: ");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
}

else if (col == "yellow5") {
  yellowminecoord[0] = xcoord;
  yellowminecoord[1] = ycoord;
  Serial.print("Coordinates of yellow mine detected on LDR5:");
  Serial.print("x-coordinate: ");
  Serial.println(yellowminecoord[0]);
  Serial.print("y-coordinate: ");
  Serial.println(yellowminecoord[1]);
  Serial.println("------------------");
  }

}

}



void loop() { if (runattempt < 4 ) {
  // put your main code here, to run repeatedly:
  while (walls_completed < 4){
    
    // While the robot has not reached the end of the wall, it should keep moving forward.
    while (cm < 160){

      // kickstart the forward movement of the robot, there should be a function instructing the robot to go backwards to hit the wall in the setup
      if (cmrear <=20){
        //AllForward();
        //delay(5000);
        while ((cmrear <= 150) && MineDetected()==false){
          // the robot moves forward steadily at a constant speed while returning the side and rear ultrasonic values
         SteadyForward();
         /*if (PassNum < 2){
          delay(1750);
          PassNum += 1;
          }
          */
         col = mine_detected();
         if (col == "yellow1" || col == "yellow2" || col == "yellow3" || col == "yellow4" || col == "yellow5") {
         digitalWrite(22, HIGH);
         delay(3000);              
         digitalWrite(22, LOW);    
         }

         if (col == "red1" || col == "red2" || col == "red3" || col == "red4" || col == "red5") {
         digitalWrite(24, HIGH);
         delay(3000);              
         digitalWrite(24, LOW);    
         }
         
         //Serial.print(col);
         step_size=20;
         
         if (MineDetected() == true){
            Serial.print(col);
            //stop the car
              AllStop();
              delay(1000);
            //record the coordinates of that mine detected

            Serial.print(col);
            
            readcoords(walls_completed, col);

             if (col == "yellow2" || col == "yellow3" || col == "yellow4"){
              //function to drive the robot forward by an appropraite distance once a yellow mine is detected
              DriveToYellowMine();
              AllStop();
              //function to close the gate to collect the yellow mine
              CloseGate();
              step_size = 20;
              } 
             else if (MineDetectedRight()==true){
              step_size = 10;
              }
          }
             //MineDetected()==false;

        }
      }

        if ((cmrear >=150) || MineDetected()==true){
          Serial.print("Ive Made it 1");
          while (cmrear >= 20){
            Serial.print("Ive Made it 2");
                myMotorL->setSpeed(150);
                myMotorR->setSpeed(150);
                BackwardDrive();
                }
        
              
              AllStop();
              delay(2500);
         

              MoveToRight(step_size);

              while (TouchTheWall() ==false){
                 AllReverse();
                 }
             
              AllStop();
              delay(2000);

              }
              SteadyForward();
              delay(1000);

              col = mine_detected();

              
        }
        // newly edited part, has not tested yet!!!!!!!!!!!!!!!!!!!--------------------------------------------------
        Rotate90C();
       delay (1000);

       AllStop();
       delay(3000);
  
       SteadyForward();
       delay(1000);

       walls_completed +=1;
       Serial.print("Walls completed: ");
       Serial.print(walls_completed);
  }

  AllStop();
  //might need to add further motor functions to make it move back to the start position
        
      }
}

      


         


/*
    while (ReachOtherSide()==false){
         // the robot moves forward steadily at a constant speed while returning the side and rear ultrasonic values
         SteadyForward();
         //write a float/integer for step size
         step_size = 20;

         // temporarily delete the mine-detected function
         //col = mine_detected();
         // if a mine is detected, carry out the following actions
        if (MineDetected == true){
            //stop the car
              AllStop();
              delay(1000);
            //record the coordinates of that mine detected
            //Coord();
             if (col == "yellow2" || col == "yellow3" || col == "yellow4"){
              //function to drive the robot forward by an appropraite distance once a yellow mine is detected
              DriveToYellowMine();
              //function to close the gate to collect the yellow mine
              GateClose();
              step_size = 20;
              }
             else if (MineDetectedRight()==true){
              step_size = 10;
              }
              while (cmrear >= 20){
                BackwardDrive();
                }
              AllStop();
              delay(2500);
    
              RHReverse90();
               delay(2100);

               AllStop();
               delay(1500);

              AllReverse();
              delay(700);

              AllStop();
              delay(1500);

              OpenGate();
              delay(1500);

              RHForward90();
              delay(2100);

              AllStop();
              delay(2500);

              while (TouchTheWall() ==false){
                 AllReverse();
                 }
             
              AllStop();
              delay(2000);
              
              }
    DriveWithoutPID();
  delay(1000);
  }

  Rotate90C();
  delay (1000);

  AllStop();
  delay(3000);
  
  DriveWithoutPID();
  delay(1000);

        }


             
            } 
        }
        //write a function that tells you if you have completed that wall
        if (WallCompleted() == true) {
          runattempt+=1;
          StartNewWall();
        }
        else {
          //write a function that tells the robot to move to the right by step_size size "step" (unless this is implemented in the reversing function)
          MoveToRight(step_size);
        }
      }      

  if (runattempt==4){
    //write a function that tells the robot to return to the start box once the algorith has run
    ReturnToStartBox();
    runattempt+=1
    }
}
*/

//part of flowchart which changed:
//write a function to identify the col that returns "red" or "yellow" and tells where to store information that follows? - and makes the appropriate lights turn on
/*          if identify
          //write a boolean for if it is only detected on the left hand side
          if (LHS_sensor_only == true) {
            //write a function to stop the car
            stop();
            //write a function to identify the col that returns "red" or "yellow" and tells where to store information that follows? - and makes the appropriate lights turn on
            if (identify_colour() == "red") {
              //write a function to record mine coordinates (if identify_colour allows you to tell it to store the coordinates for the correct colour - otherwise make two different functions for each colour)
              record_mine_coordinates();
            }
            else if (identify_colour() == "yellow") {
              record_mine_coordinates();
            }
            step = 20;
            //write a function to reverse out of the search zone, and stop once it is safely out of the search zone
            reverse_out_of_zone();
          }
          else {
            //write a boolean for if it is only detected on the right hand side
            if (RHS_sensor_only == true) {
              stop();
              if (identify_colour() == "red") {
                record_mine_coordinates();
              }
              else if (identify_colour() == "yellow") {
                record_mine_coordinates();
              }
              step = 10;
              reverse_out_of_zone();
            }
            else {
              step = 20;
              stop();
              if (identify_colour() == "yellow") {
                record_mine_coordinates();
                //write a function that drives the remaining distance to the mine in order to collect it
                drive_to_yellow_mine();
                //write a function to close gates to collect the yellow mine
                close_gates();
                reverse_out_of_zone();
                //write a function to open the gates to release the yellow mine outside of the search zone - make this function check that the mine would be safely out of the search zone when the mine is deposited
                open_gates();
              }
              else if (identify_colour() == "red") {
                record_mine_coordinates();
                reverse_out_of_zone();
              }
            }
          }
        }
        reverse_out_of_zone(); */
